import * as $ from 'jquery';
import { getLocations, getDiseases, findLocationByID,toplocations } from '../functions/index'
export default (function () {
  // Sidebar links
  $('.sidebar .sidebar-menu li a').on('click', function () {

    const $this = $(this);

    if ($this.parent().hasClass('open')) {
      $this
        .parent()
        .children('.dropdown-menu')
        .slideUp(200, () => {
          $this.parent().removeClass('open');
        });
    } else {
      $this
        .parent()
        .parent()
        .children('li.open')
        .children('.dropdown-menu')
        .slideUp(200);

      $this
        .parent()
        .parent()
        .children('li.open')
        .children('a')
        .removeClass('open');

      $this
        .parent()
        .parent()
        .children('li.open')
        .removeClass('open');

      $this
        .parent()
        .children('.dropdown-menu')
        .slideDown(200, () => {
          $this.parent().addClass('open');
        });
    }
  });
//fill sidemenu with location
getLocations().then(function(data){
  //   console.log(data);
     var menuel = $('.regionsection');
     data.forEach (function(elm){
       //console.log(elm);
       menuel.append('<li><a class="sidebar-link" href="regions.html?location='+elm.LocationID+'">'+elm.IslandName+'</a></li>');
     });
     
     var qs=parent.document.URL.substring(parent.document.URL.indexOf('='), parent.document.URL.length);
      qs =qs.substring(1, qs.length);
      findLocationByID(qs).then(function(data) {
        $('.displayregion').append(" : " + data);
      });
     });


     //get diseases
     // locad regions 
     getDiseases().then(function(data){
  //   console.log(data);
     var dmenuel = $('.diseaseselection');
     var dedesc = $('.deseasedesc');
     data.forEach (function(elm){
       dmenuel.append('<li><a class="sidebar-link" href="diseases.html?diseaseid='+elm.DiseaseCode+'">'+elm.DiseaseCode+'</a></li>');
       dedesc.html(elm.DiseaseDescription);

     });
     var qs=parent.document.URL.substring(parent.document.URL.indexOf('='), parent.document.URL.length);
     var qs =qs.substring(1, qs.length);
     console.log(qs);
$('.diseaseid').append( " : " + qs);
     });
     
     
     //top10
     toplocations().then(function(data){
       console.log("data : top10");
         var ddmenuel = $('.top10');
         var sumofpatients = 0;
         var percent_pe = 0;
         data.forEach (function(elm){
           sumofpatients+=Number(elm.numberofparientsperlocation);
           console.log(sumofpatients);
           
         });
         data.forEach (function(elm){

          percent_pe =(elm.numberofparientsperlocation/sumofpatients)*100;
          ddmenuel.append(' <div class="layer w-100 -15">\
          <h5 class="mB-5">'+elm.IslandName+'</h5>\
          <small class="fw-600 c-grey-700"></small>\
          <span class="pull-right c-grey-600 fsz-sm"></span>\
          <div class="progress mT-10">\
            <div class="progress-bar bgc-deep-purple-500" role="progressbar" aria-valuenow="'+percent_pe+'" \
            aria-valuemin="0" aria-valuemax="100" style="width:'+percent_pe+'px;">\
             <span class="sr-only">'+percent_pe+'% Reports</span></div></div></div>');

        });


         });


  // Sidebar Activity Class
  const sidebarLinks = $('.sidebar').find('.sidebar-link');

  sidebarLinks
    .each((index, el) => {
      $(el).removeClass('active');
    })
    .filter(function () {
      const href = $(this).attr('href');
      const pattern = href[0] === '/' ? href.substr(1) : href;
      return pattern === (window.location.pathname).substr(1);
    })
    .addClass('active');

  // ٍSidebar Toggle
  $('.sidebar-toggle').on('click', e => {
    $('.app').toggleClass('is-collapsed');
    e.preventDefault();
  });

  /**
   * Wait untill sidebar fully toggled (animated in/out)
   * then trigger window resize event in order to recalculate
   * masonry layout widths and gutters.
   */
  $('#sidebar-toggle').click(e => {
    e.preventDefault();
    setTimeout(() => {
      window.dispatchEvent(window.EVENT);
    }, 300);
  });
}());
